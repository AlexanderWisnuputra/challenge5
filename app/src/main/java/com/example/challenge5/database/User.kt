package com.example.challenge5.database

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "user_table")
data class User (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var email: String,
    val password: String,
    var logined: Int
    ) : Parcelable

