package com.example.challenge5

import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider

import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.challenge5.database.User
import com.example.challenge5.databinding.LayarLoginBinding
import com.example.challenge5.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.layar_login.*
import kotlinx.android.synthetic.main.layar_login.email


class Login : Fragment() {
    private lateinit var mUserViewModel: UserViewModel
    private val args by  navArgs<LoginArgs>()
    private var _binding: LayarLoginBinding? = null
    private val binding get() = _binding!!

    var a = 0
    var b = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        _binding = LayarLoginBinding.inflate(inflater, container, false)

        binding.regis.setOnClickListener {
            it.findNavController().navigate(R.id.action_login_to_registrasi)
        }
        binding.masuk.setOnClickListener {
            checkdata()
        }


        binding.tunjukkan.setOnClickListener {
            a++
            var b = a.mod(2)
            if (b == 1) {
                open()
            } else {
                clos()
            }

        }

        return binding.root

    }
    private fun checkdata() {
            val firstName = email.text.toString()
            val lastName = password.text.toString()

            if (inputCheck(firstName, lastName)) {
                val user = User(0, firstName, lastName, 1)
                mUserViewModel.getUser(user)

            } else {
                Toast.makeText(requireContext(), "Please fill out all fields.", Toast.LENGTH_SHORT)
                    .show()
            }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    private fun open() {
        binding.password.transformationMethod = HideReturnsTransformationMethod.getInstance()
        binding.tunjukkan.setBackgroundResource(R.drawable.eyes)
    }

    private fun clos() {
        binding.password.transformationMethod = PasswordTransformationMethod.getInstance()
        binding.tunjukkan.setBackgroundResource(R.drawable.closed)
    }
    private fun skip(){
        findNavController().navigate(R.id.action_login_to_home2)
    }
        private fun inputCheck(firstName: String, lastName: String): Boolean{
            return !(TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName))
        }
}
