package com.example.challenge5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.challenge5.database.User

class HomeDapter : RecyclerView.Adapter<HomeDapter.MyViewHolder>() {
    private var listUser = emptyList<User>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_main, parent, false))
    }

    override fun getItemCount(): Int {
        return listUser.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = listUser[position]

    }

    fun setData(user: List<User>) {
        this.listUser = user
        notifyDataSetChanged()
    }
}