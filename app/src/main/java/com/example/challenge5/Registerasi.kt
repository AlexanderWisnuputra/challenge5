package com.example.challenge5

import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.challenge5.database.User
import com.example.challenge5.databinding.LayarRegisBinding
import com.example.challenge5.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.layar_regis.*

class Registerasi : Fragment() {
    private var _binding: LayarRegisBinding? = null
    private lateinit var mUserViewModel: UserViewModel

    private val binding get() = _binding!!
    var re = Regex("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%!\\-_?&])(?=\\S+\$).{8,}")
    var a = 0
    var b = 0
    val adapter = HomeDapter()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        _binding = LayarRegisBinding.inflate(inflater, container, false)
        binding.savedata.setOnClickListener {
            if (binding.username.text.toString().isNotEmpty() && binding.email2.text.toString().isNotEmpty() && binding.pasuworld.text.toString().isNotEmpty()
                && binding.passworld.text.toString().isNotEmpty()
            ) {
                if (!binding.username.text.toString().contains(" ")) {
                    if (Patterns.EMAIL_ADDRESS.matcher(binding.email2.text).matches()) {
                        if (binding.pasuworld.text.toString().matches(re)) {
                            if (binding.passworld.text.toString() != binding.pasuworld.text.toString()) {
                                Toast.makeText(context, "Password tidak sesuai!", Toast.LENGTH_SHORT)
                                    .show();
                            } else {
                                insertDataToDatabase()
                                it.findNavController().navigate(R.id.action_registrasi_to_login)
                                Toast.makeText(context, "Registrasi Berhasil", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        } else {
                            Toast.makeText(
                                context,
                                "Password terdiri dari 8 huruf, Mengandung 1 Huruf Kapital, Huruf Kecil, Angka, dan Simbol!",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                    } else {
                        Toast.makeText(context, "Format Email Salah!", Toast.LENGTH_SHORT).show()
                    }
                }else{
                    Toast.makeText(context, "Username Tidak Boleh Mengandung Spasi!", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(context,"Isi semua data!",Toast.LENGTH_SHORT).show()
            }

        }
        binding.p1.setOnClickListener {
            a++
            var b = a.mod(2)
            if (b == 1) {
                open()
            }
            else {
                clos()
            }
        }
        binding.p2.setOnClickListener {
            b++
            var c = b.mod(2)
            if (c == 1) {
                open1()
            }
            else {
                clos1()
            }
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

    }
    private fun open(){
        binding.passworld.transformationMethod = HideReturnsTransformationMethod.getInstance()
        binding.p1.setBackgroundResource(R.drawable.eyes)
    }
    private fun open1(){
        binding.pasuworld.transformationMethod = HideReturnsTransformationMethod.getInstance()
        binding.p2.setBackgroundResource(R.drawable.eyes)
    }
    private fun clos(){
        binding.passworld.transformationMethod = PasswordTransformationMethod.getInstance()
        binding.p1.setBackgroundResource(R.drawable.closed)
    }
    private fun clos1(){
        binding.pasuworld.transformationMethod = PasswordTransformationMethod.getInstance()
        binding.p2.setBackgroundResource(R.drawable.closed)
    }
    private fun insertDataToDatabase() {
        val firstName = email2.text.toString()
        val lastName = passworld.text.toString()

        if(inputCheck(firstName, lastName)){
            val user = User(0, firstName, lastName,0)
            mUserViewModel.addUser(user)
        }else{
            Toast.makeText(requireContext(), "Isi semua data!", Toast.LENGTH_LONG).show()}
    }
    private fun inputCheck(firstName: String, lastName: String): Boolean{
        return !(TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName))
    }
}

